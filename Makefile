clear:
	./clear.sh

up:
	docker-compose -f docker-compose.dev.yml up --build -d

createsuperuser:
	docker container exec -it \
	iclinic-challenge_app_1 \
	python manage.py createsuperuser \
	--username="admin" --email=""

create_data:
	docker container exec -it \
	iclinic-challenge_app_1 \
	python manage.py create_data

