# iclinic-challenge

Desafio para iClinic.

https://github.com/iclinic/iclinic-python-challenge

[Desafio](challenge.md)


## Este projeto foi feito com:

* [Python 3.8.2](https://www.python.org/)
* [Django 2.2.19](https://www.djangoproject.com/)
* [Django Rest Framework 3.12.2](https://www.django-rest-framework.org/)

## Como rodar o projeto?

* Clone esse repositório.
* Crie um virtualenv com Python 3.
* Ative o virtualenv.
* Instale as dependências.
* Rode as migrações.

Sem Docker

```
git clone https://gitlab.com/rg3915/iclinic-challenge.git
cd iclinic-challenge
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
python contrib/env_gen.py

# Editar .env para DOCKER=False

python manage.py migrate
python manage.py test
python manage.py python manage.py createsuperuser --username="admin" --email=""
python manage.py create_data
python manage.py runserver
```

## Testes

```
coverage erase
coverage run manage.py test
coverage report
```


## Com Docker

```
git clone https://gitlab.com/rg3915/iclinic-challenge.git
cd iclinic-challenge
python contrib/env_gen.py

# Editar .env para DOCKER=True

docker-compose -f docker-compose.dev.yml up --build -d

make createsuperuser
make create_data
```


## POST

Meu `POST`

```
curl -X POST \
  http://0.0.0.0/prescriptions/ \
  -H 'Content-Type: application/json' \
  -d '{
  "clinic": 1,
  "physician": 1,
  "patient": 1,
  "text": "Dipirona 1x ao dia"
}'
```

![diagram.png](diagram.png)

## Links

http://www.cdrf.co/
