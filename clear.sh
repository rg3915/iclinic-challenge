# Colors
red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`

# Parando todos os containers
echo "${green}>>> Stop all containers...${reset}"
docker stop iclinic-challenge_nginx_1
docker stop iclinic-challenge_db_1
docker stop iclinic-challenge_app_1

# Deletando todos os containers
echo "${green}>>> Delete all containers...${reset}"
docker rm $(docker ps -aq)

# Limpando containers parados
echo "${green}>>> Clear containers stopped...${reset}"
docker system prune -f
docker volume prune -f
docker images -q --filter "dangling=true" | xargs -r docker rmi

echo "${green}>>> Done.${reset}"
