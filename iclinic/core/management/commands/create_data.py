from django.core.management.base import BaseCommand

from iclinic.prescription.models import Clinic, Patient, Physician


class Command(BaseCommand):
    help = 'Cria alguns dados iniciais.'

    def handle(self, *args, **options):
        self.stdout.write('Criando clínicas...')
        for i in range(1, 11):
            Clinic.objects.create(id=i)

        self.stdout.write('Criando médicos...')
        for i in range(1, 11):
            Physician.objects.create(id=i)

        self.stdout.write('Criando pacientes...')
        for i in range(1, 11):
            Patient.objects.create(id=i)
