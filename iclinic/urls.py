from django.contrib import admin
from django.urls import include, path
from rest_framework_swagger.views import get_swagger_view

from iclinic.services.urls import urlpatterns as service_urls

schema_view = get_swagger_view(title='API')

urlpatterns = [
    path('', include('iclinic.prescription.urls', namespace='prescription')),
    path('admin/', admin.site.urls),
    path('doc/', schema_view)
]

urlpatterns += service_urls
