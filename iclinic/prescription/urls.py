from django.urls import include, path
from rest_framework import routers

from iclinic.prescription.api import viewsets as v

app_name = 'prescription'

router = routers.DefaultRouter()
router.register(r'prescriptions', v.PrescriptionViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
