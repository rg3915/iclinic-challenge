from django.db import models


class Clinic(models.Model):
    id = models.AutoField(primary_key=True)

    class Meta:
        verbose_name = 'clinica'
        verbose_name_plural = 'clinicas'

    def __str__(self):
        return str(self.id)


class Physician(models.Model):
    id = models.AutoField(primary_key=True)

    class Meta:
        verbose_name = 'médico'
        verbose_name_plural = 'médicos'

    def __str__(self):
        return str(self.id)


class Patient(models.Model):
    id = models.AutoField(primary_key=True)

    class Meta:
        verbose_name = 'paciente'
        verbose_name_plural = 'pacientes'

    def __str__(self):
        return str(self.id)


class Prescription(models.Model):
    text = models.CharField(max_length=100)
    clinic = models.ForeignKey(
        Clinic,
        on_delete=models.SET_NULL,
        related_name='clinics',
        null=True,
        blank=True
    )
    physician = models.ForeignKey(
        Physician,
        on_delete=models.SET_NULL,
        related_name='physicians',
        null=True,
        blank=True
    )
    patient = models.ForeignKey(
        Patient,
        on_delete=models.SET_NULL,
        related_name='patients',
        null=True,
        blank=True
    )

    class Meta:
        ordering = ('text',)
        verbose_name = 'prescrição'
        verbose_name_plural = 'prescrições'

    def __str__(self):
        return self.text
