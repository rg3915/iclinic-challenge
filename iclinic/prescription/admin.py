from django.contrib import admin

from .models import Prescription


@admin.register(Prescription)
class PrescriptionAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'clinic', 'physician', 'patient')
    search_fields = ('text',)
