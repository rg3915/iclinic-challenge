from django.test import TestCase

from iclinic.prescription.models import (
    Clinic,
    Patient,
    Physician,
    Prescription
)


class ModelTest(TestCase):

    def setUp(self) -> None:
        self.clinic = Clinic.objects.create(id=1)
        self.physician = Physician.objects.create(id=1)
        self.patient = Patient.objects.create(id=1)
        self.prescription = Prescription.objects.create(
            text='Dipirona 1x ao dia',
            clinic=self.clinic,
            physician=self.physician,
            patient=self.patient,
        )

    def test_prescription_must_return_attributes(self):
        fields = (
            'text',
            'clinic',
            'physician',
            'patient',
        )

        for field in fields:
            with self.subTest():
                self.assertTrue(hasattr(Prescription, field))

    def test_prescription_str_must_return_text(self):
        expected = 'Dipirona 1x ao dia'
        result = str(self.prescription)
        self.assertEqual(expected, result)

    def test_physician_must_return_attributes(self):
        fields = ('id',)

        for field in fields:
            with self.subTest():
                self.assertTrue(hasattr(Physician, field))

    def test_physician_str_must_return_text(self):
        expected = '1'
        result = str(self.physician)
        self.assertEqual(expected, result)

    def test_clinic_must_return_attributes(self):
        fields = ('id',)

        for field in fields:
            with self.subTest():
                self.assertTrue(hasattr(Clinic, field))

    def test_clinic_str_must_return_text(self):
        expected = '1'
        result = str(self.clinic)
        self.assertEqual(expected, result)

    def test_patient_must_return_attributes(self):
        fields = ('id',)

        for field in fields:
            with self.subTest():
                self.assertTrue(hasattr(Patient, field))

    def test_patient_str_must_return_text(self):
        expected = '1'
        result = str(self.patient)
        self.assertEqual(expected, result)
