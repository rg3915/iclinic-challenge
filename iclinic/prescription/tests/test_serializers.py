from django.test import TestCase

from iclinic.prescription.api.serializers import PrescriptionSerializer
from iclinic.prescription.models import (
    Clinic,
    Patient,
    Physician,
    Prescription
)


class SerializerTest(TestCase):

    def setUp(self) -> None:
        self.clinic = Clinic.objects.create(id=1)
        self.physician = Physician.objects.create(id=1)
        self.patient = Patient.objects.create(id=1)
        self.prescription = Prescription.objects.create(
            text='Dipirona 1x ao dia',
            clinic=self.clinic,
            physician=self.physician,
            patient=self.patient,
        )
        self.serializer = PrescriptionSerializer(instance=self.prescription)

    def test_prescription_contains_fields_expected(self):
        data = self.serializer.data
        expected = set(['id', 'text', 'clinic', 'physician', 'patient'])
        result = set(data.keys())

        self.assertEqual(expected, result)
