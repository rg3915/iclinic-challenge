from django.test import TestCase

from iclinic.prescription.models import (
    Clinic,
    Patient,
    Physician,
    Prescription
)


class UrlTest(TestCase):

    def setUp(self) -> None:
        self.clinic = Clinic.objects.create(id=1)
        self.physician = Physician.objects.create(id=1)
        self.patient = Patient.objects.create(id=1)
        self.prescription = Prescription.objects.create(
            text='Dipirona 1x ao dia',
            clinic=self.clinic,
            physician=self.physician,
            patient=self.patient,
        )

    def test_url_prescriptions_return_status_code_200_when_(self):
        url = '/prescriptions/'

        self.r = self.client.get(url)
        self.assertEqual(200, self.r.status_code)

    def test_url_prescription_detail(self):
        url = '/prescriptions/1/'

        self.r = self.client.get(url)
        self.assertEqual(200, self.r.status_code)
