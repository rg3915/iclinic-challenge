import json

from django.test import TestCase
from rest_framework import status

from iclinic.prescription.models import (
    Clinic,
    Patient,
    Physician,
    Prescription
)


class ViewsetTest(TestCase):

    def setUp(self) -> None:
        self.clinic = Clinic.objects.create(id=1)
        self.physician = Physician.objects.create(id=1)
        self.patient = Patient.objects.create(id=1)
        self.prescription = Prescription.objects.create(
            text='Dipirona 1x ao dia',
            clinic=self.clinic,
            physician=self.physician,
            patient=self.patient,
        )

    def test_prescription_viewsets(self):
        response = self.client.get(
            '/prescriptions/',
            content_type='application/json'
        )
        result = json.loads(response.content)
        expected = [
            {
                "id": 1,
                "clinic": {
                    "id": 1
                },
                "physician": {
                    "id": 1
                },
                "patient": {
                    "id": 1
                },
                "text": "Dipirona 1x ao dia"
            }
        ]

        self.assertEqual(expected, result)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_prescription_viewsets_detail(self):
        response = self.client.get(
            '/prescriptions/1/',
            content_type='application/json'
        )
        result = json.loads(response.content)
        expected = {
            "id": 1,
            "clinic": {
                "id": 1
            },
            "physician": {
                "id": 1
            },
            "patient": {
                "id": 1
            },
            "text": "Dipirona 1x ao dia"
        }

        self.assertEqual(expected, result)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
