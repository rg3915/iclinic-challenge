from rest_framework.serializers import ModelSerializer

from iclinic.prescription.models import (
    Clinic,
    Patient,
    Physician,
    Prescription
)


class ClinicSerializer(ModelSerializer):

    class Meta:
        model = Clinic
        fields = ('id',)


class PhysicianSerializer(ModelSerializer):

    class Meta:
        model = Physician
        fields = ('id',)


class PatientSerializer(ModelSerializer):

    class Meta:
        model = Patient
        fields = ('id',)


class PrescriptionSerializer(ModelSerializer):

    class Meta:
        model = Prescription
        fields = ('id', 'text', 'clinic', 'physician', 'patient')

    def to_representation(self, instance):
        # https://stackoverflow.com/a/50257132
        self.fields['clinic'] = ClinicSerializer(read_only=True)
        self.fields['physician'] = PhysicianSerializer(read_only=True)
        self.fields['patient'] = PatientSerializer(read_only=True)
        return super(PrescriptionSerializer, self).to_representation(instance)
