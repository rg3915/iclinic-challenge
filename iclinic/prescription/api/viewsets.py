from django.http import HttpRequest
from rest_framework.viewsets import ModelViewSet

from iclinic.prescription.models import Prescription
from iclinic.services.api.viewsets import (
    ClinicsServiceViewSet,
    PatientsServiceViewSet,
    PhysiciansServiceViewSet
)
from iclinic.services.utils import Service

from .serializers import PrescriptionSerializer


def get_physician(pk):
    '''
    Get physician data from API.
    '''
    django_request = HttpRequest()
    django_request.method = 'GET'
    physicians_view = PhysiciansServiceViewSet.as_view({'get': 'retrieve'})
    data = physicians_view(request=django_request, pk=pk)
    if data:
        return data.data
    return {}


def get_clinic(pk):
    '''
    Get clinic data from API.
    '''
    django_request = HttpRequest()
    django_request.method = 'GET'
    clinics_view = ClinicsServiceViewSet.as_view({'get': 'retrieve'})
    data = clinics_view(request=django_request, pk=pk)
    if data:
        return data.data
    return {}


def get_patient(pk):
    '''
    Get patient data from API.
    '''
    django_request = HttpRequest()
    django_request.method = 'GET'
    patients_view = PatientsServiceViewSet.as_view({'get': 'retrieve'})
    data = patients_view(request=django_request, pk=pk)
    if data:
        return data.data
    return {}


def post_metrics(physician, clinic, patient):
    physician_res = get_physician(pk=physician)
    data = {}
    if physician_res:
        data['physician_id'] = physician_res['detail']['id']
        data['physician_name'] = physician_res['detail']['name']
        data['physician_crm'] = physician_res['detail']['crm']

    clinic_res = get_clinic(pk=clinic)
    if clinic_res:
        data['clinic_id'] = clinic_res['detail']['id']
        data['clinic_name'] = clinic_res['detail']['name']

    patient_res = get_patient(pk=patient)
    if patient_res:
        data['patient_id'] = patient_res['detail']['id']
        data['patient_name'] = patient_res['detail']['name']
        data['patient_email'] = patient_res['detail']['email']
        data['patient_phone'] = patient_res['detail']['phone']

    response = Service.post_metrics(data)
    if response:
        return True
    return False


class PrescriptionViewSet(ModelViewSet):
    queryset = Prescription.objects.all()
    serializer_class = PrescriptionSerializer

    def create(self, request, *args, **kwargs):
        physician = request.data['physician']
        clinic = request.data['clinic']
        patient = request.data['patient']

        metrics = post_metrics(physician, clinic, patient)
        if metrics:
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
        return super(PrescriptionViewSet, self).create(request, *args, **kwargs)
