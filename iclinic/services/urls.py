from django.urls import include, path
from rest_framework import routers

from .api import viewsets as v

router = routers.DefaultRouter()
router.register('physicians-service', v.PhysiciansServiceViewSet, basename='physicians')
router.register('clinics-service', v.ClinicsServiceViewSet, basename='clinics')
router.register('patients-service', v.PatientsServiceViewSet, basename='patients')

urlpatterns = [
    path('', include(router.urls)),
]
