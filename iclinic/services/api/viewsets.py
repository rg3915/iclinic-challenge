from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet

from ..utils import Service, ServiceException


class PhysiciansServiceViewSet(ViewSet):

    def retrieve(self, request, pk=None):
        try:
            data = Service.get_physicians(pk)
            return Response({'detail': data.json()})
        except ServiceException as e:
            return Response({'detail': f'{e}'}, status=status.HTTP_400_BAD_REQUEST)


class ClinicsServiceViewSet(ViewSet):

    def retrieve(self, request, pk=None):
        try:
            data = Service.get_clinics(pk)
            return Response({'detail': data.json()})
        except ServiceException as e:
            return Response({'detail': f'{e}'}, status=status.HTTP_400_BAD_REQUEST)


class PatientsServiceViewSet(ViewSet):

    def retrieve(self, request, pk=None):
        try:
            data = Service.get_patients(pk)
            return Response({'detail': data.json()})
        except ServiceException as e:
            return Response({'detail': f'{e}'}, status=status.HTTP_400_BAD_REQUEST)
