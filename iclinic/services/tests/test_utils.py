import unittest

from ..utils import Service, ServiceException


class PhysicianTests(unittest.TestCase):

    def test_request_response(self):
        response = Service.get_physicians(pk=1)
        self.assertEqual(response.status_code, 200)

    def test_should_raise_exception_service_exception_when_response_status_code_different_200(self):  # noqa
        with self.assertRaises(ServiceException):
            response = Service.get_physicians(pk=1)
            # fazer mock


class ClinicTests(unittest.TestCase):

    def test_request_response(self):
        response = Service.get_clinics(pk=1)
        self.assertEqual(response.status_code, 200)


class PatientTests(unittest.TestCase):

    def test_request_response(self):
        response = Service.get_patients(pk=1)
        self.assertEqual(response.status_code, 200)


class MetricTests(unittest.TestCase):

    def test_request_response(self):
        data = {
            "clinic_id": 1,
            "clinic_name": "Clínica A",
            "physician_id": 1,
            "physician_name": "José",
            "physician_crm": "SP293893",
            "patient_id": 1,
            "patient_name": "Rodrigo",
            "patient_email": "rodrigo@gmail.com",
            "patient_phone": "(16)998765625"
        }
        response = Service.post_metrics(data=data)
        self.assertEqual(response.status_code, 201)
