import requests
from decouple import config
from rest_framework import status

API_TOKEN = config('API_TOKEN')

URL = 'https://5f71da6964a3720016e60ff8.mockapi.io/v1'


class ServiceException(Exception):
    pass


class Service:
    DEFAULT_HEADERS = {'Authorization': f'Bearer {API_TOKEN}'}
    DEFAULT_TIMEOUT = 5

    @classmethod
    def get_physicians(cls, pk):
        url = f'{URL}/physicians/{pk}'
        session = requests.Session()
        session.auth = ('Authorization', f'Bearer {API_TOKEN}')
        adapter = requests.adapters.HTTPAdapter(max_retries=3)
        session.mount(url, adapter)

        response = session.get(
            url,
            timeout=cls.DEFAULT_TIMEOUT,
            verify=False
        )
        if response.status_code == status.HTTP_200_OK:
            return response
        else:
            raise ServiceException(f'physician not found. Status: {response.status_code}')

    @classmethod
    def get_clinics(cls, pk):
        url = f'{URL}/clinics/{pk}'
        session = requests.Session()
        session.auth = ('Authorization', f'Bearer {API_TOKEN}')
        adapter = requests.adapters.HTTPAdapter(max_retries=3)
        session.mount(url, adapter)

        response = session.get(
            url,
            timeout=cls.DEFAULT_TIMEOUT,
            verify=False
        )
        if response.status_code == status.HTTP_200_OK:
            return response
        else:
            raise ServiceException(f'clinic not found. Status: {response.status_code}')

    @classmethod
    def get_patients(cls, pk):
        url = f'{URL}/patients/{pk}'
        session = requests.Session()
        session.auth = ('Authorization', f'Bearer {API_TOKEN}')
        adapter = requests.adapters.HTTPAdapter(max_retries=3)
        session.mount(url, adapter)

        response = session.get(
            url,
            timeout=cls.DEFAULT_TIMEOUT,
            verify=False
        )
        if response.status_code == status.HTTP_200_OK:
            return response
        else:
            raise ServiceException(f'patient not found. Status: {response.status_code}')

    @classmethod
    def post_metrics(cls, data):
        url = f'{URL}/metrics'
        response = requests.post(
            url,
            headers=cls.DEFAULT_HEADERS,
            timeout=cls.DEFAULT_TIMEOUT,
            verify=False
        )
        if response.status_code == status.HTTP_201_CREATED:
            return response
        else:
            raise ServiceException(f'Error. Status: {response.status_code}')
